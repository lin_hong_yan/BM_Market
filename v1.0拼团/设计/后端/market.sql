/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/7/3 16:23:54                            */
/*==============================================================*/


drop table if exists m_banner;

drop table if exists m_groupbuy_conf;

drop table if exists m_groupbuy_info;

drop table if exists m_groupbuy_offered;

drop table if exists m_groupbuy_offered_user;

drop table if exists m_item_image;

drop table if exists m_item_info;

drop table if exists m_order_detail;

drop table if exists m_order_info;

drop table if exists m_order_status_log;

drop table if exists m_product_group;

drop table if exists m_product_image;

drop table if exists m_product_info;

drop table if exists m_stall;

drop table if exists m_stall_activity;

drop table if exists m_user_address;

drop table if exists m_user_info;

/*==============================================================*/
/* Table: m_banner                                              */
/*==============================================================*/
create table m_banner
(
   id                   bigint(20) not null auto_increment comment '主键',
   code                 varchar(32) default NULL comment '编码',
   type                 varchar(32) default NULL comment '类型',
   title                varchar(256) default NULL comment '标题',
   url_type             varchar(32) default NULL comment '跳转类型',
   pic_url              varchar(512) default NULL comment '展示图URL',
   web_url              varchar(256) default NULL comment '链接页面URL',
   web_title            varchar(64) default NULL comment '链接页面标题',
   web_desc             varchar(256) default NULL comment '链接页面描述',
   url_code             varchar(32) default NULL comment 'url指向页面Code（内部页面）',
   url_param            varchar(256) default NULL comment '内部跳转参数',
   status               varchar(1) default NULL comment '是否有效（1有效，2关闭）',
   start_time           datetime default NULL comment '生效开始时间',
   end_time             datetime default NULL comment '生效结束时间',
   position             int default NULL comment '排序优先级',
   create_code          varchar(32) comment '创建人',
   create_name          varchar(64) comment '创建人名称',
   create_time          datetime default NULL comment '创建时间',
   op_code              varchar(32) default NULL comment '操作人',
   op_name              varchar(64) default NULL comment '操作人编号',
   op_time              datetime default NULL comment '操作时间',
   primary key (id)
);

alter table m_banner comment 'banner表';

/*==============================================================*/
/* Table: m_groupbuy_conf                                       */
/*==============================================================*/
create table m_groupbuy_conf
(
   id                   bigint not null auto_increment comment '主键',
   title                varchar(128) comment '标题',
   content              text comment '内容',
   op_by                varchar(32) comment '操作人',
   op_name              varchar(64) comment '操作人名称',
   op_time              datetime comment '操作时间',
   primary key (id)
);

alter table m_groupbuy_conf comment '团购规则配置';

/*==============================================================*/
/* Table: m_groupbuy_info                                       */
/*==============================================================*/
create table m_groupbuy_info
(
   gb_id                bigint not null auto_increment comment '团购活动ID',
   code                 varchar(32) default NULL comment '商品编号',
   group_name           varchar(64) default NULL comment '商品名称',
   gb_name              varchar(64) comment '活动名称',
   status               varchar(6) comment '状态',
   gb_type              varchar(1) comment '团购类型(1官方团；2个人团)',
   group_number         int comment '成团人数',
   start_time           datetime comment '开始时间',
   end_time             datetime comment '结束时间',
   aging                int comment '成团时效(小时)',
   postage              int default NULL comment '邮费(分,免邮为0)',
   item_id              bigint comment '商品ID',
   valid                varchar(1) comment '是否有效(1:有效；0:无效)',
   create_code          varchar(32) default NULL comment '创建人',
   create_name          varchar(64) comment '创建人名称',
   create_time          datetime default NULL comment '创建时间',
   op_code              varchar(32) default NULL comment '修改人',
   op_name              varchar(64) comment '操作人名称',
   op_time              datetime default NULL comment '修改时间',
   primary key (gb_id)
);

alter table m_groupbuy_info comment '团购活动信息表';

/*==============================================================*/
/* Table: m_groupbuy_offered                                    */
/*==============================================================*/
create table m_groupbuy_offered
(
   gb_offered_id        bigint not null auto_increment comment '参团ID',
   stall_activity_id    bigint comment '摊位上架活动ID',
   status               varchar(16) comment '参团状态',
   start_time           datetime comment '开始时间',
   end_time             datetime comment '结束时间',
   offered_time         datetime comment '成团时间',
   gb_type              varchar(1) comment '团购类型(1官方团；2个人团)',
   user_id              bigint comment '团长用户id',
   group_number         int comment '成团人数',
   offered_number       int comment '已参与人数',
   primary key (gb_offered_id)
);

alter table m_groupbuy_offered comment '团购活动参团信息表';

/*==============================================================*/
/* Table: m_groupbuy_offered_user                               */
/*==============================================================*/
create table m_groupbuy_offered_user
(
   id                   bigint not null auto_increment comment '主键',
   gb_offered_id        bigint comment '参团ID',
   user_id              bigint comment '用户ID',
   user_type            varchar(6) comment '参与人类型(1团长;2参与人)',
   create_time          datetime comment '参与时间',
   primary key (id)
);

alter table m_groupbuy_offered_user comment '团购活动参团用户表';

/*==============================================================*/
/* Table: m_item_image                                          */
/*==============================================================*/
create table m_item_image
(
   id                   bigint not null auto_increment comment 'id',
   item_id              bigint(20) default NULL comment '商品编号',
   img_type             varchar(32) default NULL comment '图片类型',
   url                  varchar(1024) default NULL comment '图片URL',
   valid                varchar(1) default NULL comment '是否有效',
   create_code          varchar(32) default NULL comment '创建人',
   create_time          datetime default NULL comment '创建时间',
   op_code              varchar(32) default NULL comment '修改人',
   op_time              datetime default NULL comment '修改时间',
   primary key (id)
);

alter table m_item_image comment '商品图片表';

/*==============================================================*/
/* Table: m_item_info                                           */
/*==============================================================*/
create table m_item_info
(
   item_id              bigint(20) not null auto_increment comment 'id',
   code                 varchar(32) default NULL comment '商品编号',
   name                 varchar(64) default NULL comment '商品名称',
   member_price         int default NULL comment '活动价(单位：分)',
   limit_num            int comment '产品总量',
   sell_num             int comment '总销售量',
   item_desc            varchar(512) comment '商品描述',
   detail_type          varchar(1) comment '详情类型(1:图片；2：文本)',
   detail               text comment '商品详情',
   product_code         varchar(32) comment '产品编号',
   product_name         varchar(64) comment '产品简称',
   market_price         int comment '市场价(单位：分)',
   product_info         text comment '产品主数据(json)',
   create_code          varchar(32) default NULL comment '创建人',
   create_name          varchar(64) comment '创建人名称',
   create_time          datetime default NULL comment '创建时间',
   op_code              varchar(32) default NULL comment '修改人',
   op_name              varchar(64) comment '操作人名称',
   op_time              datetime default NULL comment '修改时间',
   primary key (item_id)
);

alter table m_item_info comment '商品表';

/*==============================================================*/
/* Table: m_order_detail                                        */
/*==============================================================*/
create table m_order_detail
(
   order_detail_id      bigint not null auto_increment comment 'id',
   order_id             bigint default NULL comment '订单ID',
   order_status         varchar(32) default NULL comment '状态',
   stall_activity_id    bigint comment '摊位上架活动ID',
   activity_type        varchar(2) comment '活动类型(1团购，2秒杀，3砍价)',
   activity_id          bigint comment '活动id',
   group_name           varchar(64) comment '团名称',
   item_id              bigint(20) default NULL comment '商品ID',
   item_num             int default NULL comment '商品数量',
   item_name            varchar(64) default NULL comment '商品名称',
   pic_url              varchar(1024) default NULL comment '商品图片',
   member_price         int default NULL comment '活动价(单位：分)',
   market_price         int default NULL comment '市场价(单位：分)',
   refund_code          varchar(32) default NULL comment '退款号',
   refund_status        varchar(32) default NULL comment '退款状态',
   op_code              varchar(32) default NULL comment '操作人',
   op_name              varchar(64) comment '操作人名称',
   op_time              datetime default NULL comment '操作时间',
   gift_flag            varchar(1) default '0' comment '是否赠品(1是；0否)',
   real_price           int default NULL comment '实付金额',
   part_discount_fee    int default 0 comment '优惠分摊',
   primary key (order_detail_id),
   key idx_orderId (order_id)
);

alter table m_order_detail comment '订单详情表';

/*==============================================================*/
/* Table: m_order_info                                          */
/*==============================================================*/
create table m_order_info
(
   order_id             bigint not null auto_increment comment 'id',
   order_code           varchar(32) default NULL comment '订单编码',
   order_status         varchar(32) default NULL comment '订单状态',
   pay_type             varchar(32) default NULL comment '支付方式',
   pay_time             datetime default NULL comment '付款时间',
   end_pay_time         datetime comment '有效支付结束时间',
   all_num              int default NULL comment '总数量',
   maket_price          int default NULL comment '总市场价',
   member_price         int default NULL comment '总进货价',
   total_discount       int default NULL comment '总优惠金额',
   postage              int comment '邮费（分）',
   pay_amount           int default NULL comment '实付金额',
   user_id              bigint(20) default NULL comment '消费者ID',
   address_id           varchar(32) default NULL comment '消费者地址ID',
   receive_name         varchar(32) default NULL comment '收货人姓名',
   receive_mobile       varchar(32) default NULL comment '收货人手机号',
   province_id          varchar(32) default NULL comment '省份ID',
   province             varchar(32) default NULL comment '省份',
   city_id              varchar(32) default NULL comment '城市ID',
   city                 varchar(32) default NULL comment '城市',
   county_id            varchar(32) default NULL comment '区县ID',
   county               varchar(32) default NULL comment '区县',
   address              varchar(128) default NULL comment '详细地址',
   create_time          datetime default NULL comment '创建时间',
   send_status          varchar(32) default NULL comment '发货状态',
   send_time            datetime default NULL comment '发货时间',
   receive_time         datetime default NULL comment '签收时间',
   over_time            datetime default NULL comment '结单时间',
   user_remark          varchar(256) default NULL comment '买家备注',
   seller_remark        varchar(256) default NULL comment '卖家备注',
   pay_water_code       varchar(32) default NULL comment '第三方支付流水号',
   stall_activity_id    bigint comment '摊位上架活动ID',
   op_code              varchar(32) default NULL comment '操作人',
   op_name              varchar(64) default NULL comment '操作人名称',
   op_time              datetime default NULL comment '操作时间',
   primary key (order_id)
);

alter table m_order_info comment '订单表';

/*==============================================================*/
/* Table: m_order_status_log                                    */
/*==============================================================*/
create table m_order_status_log
(
   id                   bigint(20) not null auto_increment comment '主键ID',
   order_id             bigint(20) default NULL comment '订单ID',
   old_status           varchar(32) default NULL comment '原状态',
   new_status           varchar(32) default NULL comment '新状态',
   up_time              datetime default NULL comment '更新时间',
   up_code              varchar(32) default NULL comment '更新人',
   primary key (id)
);

alter table m_order_status_log comment '订单状态流转信息表';

/*==============================================================*/
/* Table: m_product_group                                       */
/*==============================================================*/
create table m_product_group
(
   id                   bigint not null auto_increment comment 'id',
   parent_id            bigint comment '组合编码',
   child_id             bigint comment '组合下面产品编码',
   num                  int comment '数量',
   valid                varchar(1) comment '是否有效(1：有效 ，0 ：无效)',
   created_code         varchar(32) comment '创建人',
   created_time         datetime comment '创建时间',
   op_code              varchar(32) comment '修改人',
   op_time              datetime comment '修改时间',
   primary key (id)
);

alter table m_product_group comment '组合产品关系表';

/*==============================================================*/
/* Table: m_product_image                                       */
/*==============================================================*/
create table m_product_image
(
   id                   bigint(20) not null auto_increment comment 'id',
   product_id           bigint(20) default NULL comment '产品编号',
   img_type             varchar(32) default NULL comment '图片类型',
   url                  varchar(1024) default NULL comment '图片URL',
   valid                varchar(1) default NULL comment '是否有效',
   create_code          varchar(32) default NULL comment '创建人',
   create_time          datetime default NULL comment '创建时间',
   op_code              varchar(32) default NULL comment '修改人',
   op_time              datetime default NULL comment '修改时间',
   primary key (id)
);

alter table m_product_image comment '产品图片表';

/*==============================================================*/
/* Table: m_product_info                                        */
/*==============================================================*/
create table m_product_info
(
   product_id           bigint not null auto_increment comment 'id',
   product_code         varchar(20) comment '产品编码',
   product_name         varchar(60) comment '产品名称',
   short_name           varchar(60) comment '产品简称',
   carton               int comment '规格',
   unit                 varchar(20) comment '单位',
   market_price         int comment '市场价(单位：分)',
   valid                varchar(1) comment '是否有效，软删除(1:有效；0：无效)',
   product_category     varchar(20) comment '品类',
   status               varchar(1) comment '状态(1：启用，0：禁用)',
   product_type         varchar(4) comment '产品类型(组合产品：ZHCP，单产品:DCP)',
   remark               varchar(200) comment '备注',
   create_code          varchar(32) comment '创建人',
   create_name          varchar(64) comment '创建人姓名',
   create_time          datetime comment '创建时间',
   op_name              varchar(32) comment '操作人姓名',
   op_code              varchar(64) comment '操作人',
   op_time              datetime comment '操作时间',
   primary key (product_id)
);

alter table m_product_info comment '产品主数据表';

/*==============================================================*/
/* Table: m_stall                                               */
/*==============================================================*/
create table m_stall
(
   stall_id             bigint not null auto_increment comment 'id',
   type                 char(10) comment '类型',
   name                 varchar(20) comment '摊位编码',
   position             int comment '排序(不重复)',
   valid                varchar(1) comment '是否有效（1：有效  0：无效）',
   create_code          varchar(32) comment '创建人',
   create_time          datetime comment '创建时间',
   op_name              varchar(64) comment '操作人姓名',
   op_code              varchar(32) comment '操作人',
   op_time              datetime comment '操作时间',
   primary key (stall_id)
);

alter table m_stall comment '摊位配置表';

/*==============================================================*/
/* Table: m_stall_activity                                      */
/*==============================================================*/
create table m_stall_activity
(
   stall_activity_id    bigint not null auto_increment comment '摊位上架活动ID',
   online_code          varchar(14) comment '上架编号',
   stall_id             bigint comment '摊位ID',
   offline_status       varchar(1) comment '下线状态(1正常;2已下线)',
   start_time           datetime comment '开始时间',
   end_time             datetime comment '结束时间',
   activity_type        varchar(6) comment '活动类型(1团购,2秒杀,3砍价)',
   activity_id          bigint comment '活动ID',
   activity_code        varchar(20) comment '活动编码',
   limit_sell_num       int comment '本次可销售量',
   limit_num            int comment '本次可购买上限',
   sell_num             int comment '销量（件）',
   name                 varchar(64) comment '名称',
   product_name         varchar(64) comment '产品简称',
   member_price         int comment '销售价（活动价）(分)',
   valid                varchar(1) comment '是否有效（1：有效，0：无效）',
   pic_url              varchar(1024) comment '封面图片URL',
   create_code          varchar(32) comment '创建人',
   create_name          varchar(64) comment '创建人名称',
   create_time          datetime comment '创建时间',
   op_code              varchar(32) comment '操作人',
   op_name              varchar(64) comment '操作人姓名',
   op_time              datetime comment '操作时间',
   primary key (stall_activity_id)
);

alter table m_stall_activity comment '摊位上架活动信息表';

/*==============================================================*/
/* Table: m_user_address                                        */
/*==============================================================*/
create table m_user_address
(
   address_id           bigint(20) not null auto_increment comment 'id',
   user_id              bigint(20) default NULL comment '用户ID',
   receive_name         varchar(32) default NULL comment '收货人姓名',
   receive_mobile       varchar(32) default NULL comment '收货人手机号',
   province_id          varchar(32) default NULL comment '省份ID',
   province             varchar(32) default NULL comment '省份',
   city_id              varchar(32) default NULL comment '城市ID',
   city                 varchar(32) default NULL comment '城市',
   county_id            varchar(32) default NULL comment '区县ID',
   county               varchar(32) default NULL comment '区县',
   address              varchar(128) default NULL comment '详细地址',
   default_flag         varchar(1) default NULL comment '是否默认',
   valid                varchar(1) default NULL comment '是否有效',
   create_time          datetime default NULL comment '创建时间',
   op_time              datetime default NULL comment '操作时间',
   primary key (address_id),
   key idx_userId (user_id)
);

alter table m_user_address comment '用户收货地址表';

/*==============================================================*/
/* Table: m_user_info                                           */
/*==============================================================*/
create table m_user_info
(
   user_id              bigint(20) not null auto_increment comment '主键',
   openid               varchar(64) default NULL comment 'openId',
   unionid              varchar(64) default NULL comment 'unionid',
   nick_name            varchar(64) default NULL comment '用户昵称',
   mobile               varchar(32) default NULL comment '用户手机号',
   age                  int default NULL comment '用户年龄',
   sex                  varchar(11) default NULL comment '用户性别(1男；2女；3未知)',
   type                 int default NULL comment '用户类型',
   avatar_url           varchar(255) default NULL comment '头像',
   create_time          datetime default NULL comment '创建时间',
   city                 varchar(128) default NULL comment '用户所在城市',
   country              varchar(128) comment 'country',
   language             varchar(64) default NULL comment '用户的语言',
   province             varchar(128) default NULL comment '用户所在省份',
   primary key (user_id)
);

alter table m_user_info comment '用户信息表';

