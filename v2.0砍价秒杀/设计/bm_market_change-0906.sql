CREATE INDEX `idx_item` ON `m_activity_message`(`item_id`, `item_status`) USING BTREE ;

CREATE TABLE `m_bargain_info` (
`bn_id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动编码' ,
`m_explain`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动说明' ,
`status`  tinyint(1) NULL DEFAULT NULL COMMENT '状态：0关闭；1草稿；2可用' ,
`start_time`  datetime NULL DEFAULT NULL COMMENT '开始时间' ,
`end_time`  datetime NULL DEFAULT NULL COMMENT '结束时间' ,
`discount`  int(11) NULL DEFAULT NULL COMMENT '首刀百分比' ,
`bn_number`  int(11) NULL DEFAULT NULL COMMENT '刀数' ,
`aging`  int(11) NULL DEFAULT NULL COMMENT '砍价时效' ,
`postage`  int(11) NULL DEFAULT NULL COMMENT '邮费（分）' ,
`item_id`  bigint(20) NULL DEFAULT NULL COMMENT '商品ID' ,
`valid`  tinyint(1) NULL DEFAULT NULL COMMENT '是否有效(1:有效；0:无效)' ,
`create_code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人' ,
`create_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`op_code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人' ,
`op_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人名称' ,
`op_time`  datetime NULL DEFAULT NULL COMMENT '操作时间' ,
PRIMARY KEY (`bn_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='砍价活动信息表'
;

CREATE TABLE `m_bargain_offered` (
`bn_offered_id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '砍价发起Id' ,
`bn_id`  bigint(20) NULL DEFAULT NULL COMMENT '砍价活动id' ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动编码' ,
`stall_activity_id`  bigint(20) NULL DEFAULT NULL COMMENT '摊位上架活动ID' ,
`online_code`  varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摊位上架活动编码' ,
`openid`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请砍价人openid' ,
`status`  tinyint(1) NULL DEFAULT NULL COMMENT '状态(0:已关闭;1：已成功;2：砍价中;3：已领取；' ,
`market_price`  int(11) NULL DEFAULT NULL COMMENT '总金额（分）' ,
`bargain_price`  int(11) NULL DEFAULT NULL COMMENT '已砍金额(分)' ,
`bargain_count`  int(11) NULL DEFAULT NULL COMMENT '已帮砍次数' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`op_time`  datetime NULL DEFAULT NULL COMMENT '操作时间' ,
`end_time`  datetime NULL DEFAULT NULL COMMENT '团最终的有效时间' ,
PRIMARY KEY (`bn_offered_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='砍价信息发起表'
;

CREATE TABLE `m_bargain_offered_user` (
`bn_help_id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '帮砍主键id' ,
`bn_offered_id`  bigint(20) NULL DEFAULT NULL COMMENT '砍价发起Id' ,
`openid`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人openid' ,
`help_openid`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮砍价人openid' ,
`help_name`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '帮砍价人姓名' ,
`help_text`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮砍价人显示文案' ,
`help_pic`  varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮砍价人头像' ,
`bargain_price`  int(11) NULL DEFAULT NULL COMMENT '砍价的金额（分）' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
PRIMARY KEY (`bn_help_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='帮砍价信息表'
;

CREATE INDEX `idx_gb_offered_id` ON `m_groupbuy_offered_user`(`gb_offered_id`) USING BTREE ;

CREATE INDEX `idx_gb_offered_id` ON `m_order_detail`(`gb_offered_id`, `order_status`) USING BTREE ;

CREATE TABLE `m_seckill_info` (
`sk_id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动编码' ,
`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动名称' ,
`m_explain`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动说明' ,
`status`  tinyint(1) NULL DEFAULT NULL COMMENT '状态：0关闭；1草稿；2可用' ,
`start_time`  datetime NULL DEFAULT NULL COMMENT '开始时间' ,
`end_time`  datetime NULL DEFAULT NULL COMMENT '结束时间' ,
`postage`  int(11) NULL DEFAULT NULL COMMENT '邮费（分）' ,
`item_id`  bigint(20) NULL DEFAULT NULL COMMENT '商品ID' ,
`valid`  tinyint(1) NULL DEFAULT NULL COMMENT '是否有效(1:有效；0:无效)' ,
`create_code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人' ,
`create_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`op_code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人' ,
`op_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人名称' ,
`op_time`  datetime NULL DEFAULT NULL COMMENT '操作时间' ,
PRIMARY KEY (`sk_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='秒杀活动信息表'
;

CREATE TABLE `m_seckill_remind` (
`id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键' ,
`stall_activity_id`  bigint(20) NULL DEFAULT NULL COMMENT '摊位上架活动id' ,
`openid`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户openId' ,
`item_desc`  varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品描述' ,
`start_time`  datetime NULL DEFAULT NULL COMMENT '秒杀开始时间' ,
`has_notice`  tinyint(1) NULL DEFAULT NULL COMMENT '是否已通知' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`op_time`  datetime NULL DEFAULT NULL COMMENT '操作时间' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='秒杀活动提醒表'
;

ALTER TABLE `m_stall_activity` ADD COLUMN `seckill_time`  datetime NULL DEFAULT NULL COMMENT '秒杀开始时间' AFTER `op_time`;

ALTER TABLE `m_stall_activity` ADD COLUMN `seckill_pic`  varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '秒杀预告图片' AFTER `seckill_time`;

